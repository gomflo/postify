module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		connect: {
			use_defaults: {}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-connect');
}