app.factory('Flickr', ['$http', 'FLICKR_API_KEY', function($http, FLICKR_API_KEY){
	var baseUrl = 'https://api.flickr.com/services/rest';
	
	return {
		search: function(tags) {
			return $http.jsonp(baseUrl, { 
				params: {
					api_key: FLICKR_API_KEY, 
					method: 'flickr.photos.search',
					group_id: '1463451@N25',
					text: tags,
					jsoncallback: 'JSON_CALLBACK',
					format: 'json'
				}
			});
		}
	};
}])

app.factory('Zip', ['$resource', '$routeParams', function($resource, $routeParams){
	var resourceUrl = 'http://localhost:3000/zip/:id';
	return $resource(resourceUrl, { id: $routeParams.id });
}])