app.controller('MainCtrl', ['$scope', '$http', '$timeout', function($scope, $http, $timeout) {
	
	var delay;
	$scope.search = function(query) {
		$timeout.cancel(delay);

		delay = $timeout(function(){
			$scope.loading = true;

			$timeout(function(){
				$http.get('http://localhost:3000/search/', { params: { s: query } })
				.success(function(response){
					$timeout(function(){
						$scope.codes = response;
						$scope.loading = false;
					});
				});
			}, 3000)
		}, 700);
	}
}]);

app.controller('ZipCtrl', ['$scope', 'Zip', 'Flickr', function($scope, Zip, Flickr) {	

	Zip.get(function(response) {
		var zip = $scope.zip = response;

		var tags = [zip.estado, 'Mexico'].join(' ');
		Flickr.search(tags).success(function(response) {
			if (response.photos.photo.length) {
				var photos = $scope.photos = response.photos.photo;
				$scope.$emit('PHOTOS_LOADED', photos);
			}
		});
	});
}]);

app.controller('StatesCtrl', ['$scope', '$http', function($scope, $http){
	$http.get('http://localhost:3000/states/')
	.success(function(response){
		$scope.states = response
	})
}]);