app.directive('flickrBackground', function($timeout){
	var randomNumber = function(n) {
		return Math.floor(Math.random() * n);
	}
	var formatFlickrUrl = function(farm, server, id, secret, size) {
		return 'http://farm'
		.concat(farm)
		.concat('.staticflickr.com/')
		.concat(server)
		.concat('/')
		.concat(id)
		.concat('_')
		.concat(secret)
		.concat('_b.jpg');
	}
	return {
		restrict: 'A',
		link: function(scope, element, attributes) {
			scope.loadingFlickrImage = true;
			scope.$on('PHOTOS_LOADED', function(event, photos) {
				var random = randomNumber(photos.length),
						image = document.createElement('img'),
						photo = photos[random],
						url =  formatFlickrUrl(photo.farm, photo.server, photo.id, photo.secret);

				var preLoadImage = image.setAttribute('src', url);
				angular.element(image).bind('load', function(){
					$timeout(function(){
						scope.$apply(function(){
							scope.loadingFlickrImage = false;
						});
						element.css('background-image', 'url('+url+')');
					}, 3000);
				});
				scope.photo = photo;
			})
		}
	}
})