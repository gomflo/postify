var app = angular.module('PostalCodes', [
	'ngRoute',
	'ngResource',
	'ngAnimate'
]);

app
.constant('FLICKR_API_KEY', '09ddb3cbc5c4d0b46d71196bf7a9c538')
.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {

	$routeProvider.when('/zip/:id', { controller: 'ZipCtrl', templateUrl: 'views/zip.html' });
	$routeProvider.when('/', { controller: 'MainCtrl', templateUrl: 'views/main.html' });
	$routeProvider.otherwise({ redirectTo: '/' });
}]);

angular.element(document).ready(function(){
	angular.bootstrap(document, ['PostalCodes']);	
});